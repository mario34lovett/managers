## Background

* To facilitate participation in data-related conferences, both as speakers and attendees, this page is meant as a list of possible conferences of interest.  If you know of conferences, please make a merge request! 

## Conferences

| Name | URL | Dates | Call for Speakers Deadline | Location(s) |
| ----------- | ----------- | ----------- | ----------- | ----------- |
| DBT Coalesce | https://www.getdbt.com/coalesce/ | AUG 20-21, 2020 | FEB 28, 2020 | New York City |
| Data Council | https://www.datacouncil.ai/ | April 7-8,2020 | Late Submissions Accepted | San Francisco |
| AWS Reinvent | https://reinvent.awsevents.com/ | Nov 30-Dec 4, 2020 | | |
| Disney Data Conference | https://disneydataconference.com/ | Aug 18-19, 2020 | | |
| CSV Conf | https://csvconf.com/ | May 13-14, 2020 | Feb 9, 2020 | Washington, DC |